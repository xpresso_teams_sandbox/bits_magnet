"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger
import pickle
import os
import json
import pandas as pd
import numpy as np
import xgboost as xgb
#from sklearn.preprocessing import PolynomialFeatures

__author__ = "### Author ###"

logger = XprLogger("prediction",level=logging.INFO)


class Prediction(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """

        # load the model to disk
        with open(os.path.join(model_path, 'boosted_models.sav'), 'rb') as f:
            self.models = pickle.load(f)
 
        print('boosted_models.sav Model loaded')



        # load metadata to the disk
        meta_file = os.path.join(model_path, "meta.json")
        with open(meta_file, "r") as f:
            self.meta_dict = json.load(f)

        print('meta.json loaded')

        self.last_date = self.meta_dict['last_date']
        self.last_index = self.meta_dict['last_index']

        #self.date_diff = (pd.datetime.today() - pd.to_datetime(self.last_date)).days
        self.date_list = [pd.to_datetime(self.last_date) + pd.DateOffset(i+1) for i in range(14)]


    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """

        print("Transform input running")
        print(input_request)

        input_request['test_data_file'] = os.path.join('/inf_data', input_request['test_data_file'])
        input_request['predictions_location'] = os.path.join('/inf_data', input_request['predictions_location'])

        return input_request


    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        start_index = 1 + self.last_index
        index_to_predict = range(start_index, start_index + 14)
        x_pred = np.array(index_to_predict).reshape((14, 1))
        #x_pred = self.poly_feature.fit_transform(x_pred)

        print("Inside Predict Function")

        test_df = pd.read_excel(input_request['test_data_file'])
        countries = test_df.Country.tolist()


        predict_df = pd.DataFrame(columns=['Country', 'Date', 'Confirmed'])
        for country in countries:
            model = self.models[country]
            pred = model.predict(x_pred)
            pred = pred.flatten()
            # predict_dict[country] = pred
            country_list = [country]*14
            temp = pd.DataFrame(list(zip(country_list, self.date_list, pred)), columns=['Country', 'Date', 'Confirmed'])
            predict_df = pd.concat([predict_df, temp])
   
        predict_df['Confirmed'] = predict_df['Confirmed'].abs()
        predict_df.reset_index(inplace=True, drop=True )
        # print(predict_df.tail())

        if not os.path.exists(input_request['predictions_location']):
            os.makedirs(input_request['predictions_location'])
        predict_df.to_csv(os.path.join(input_request['predictions_location'], 'predictions.csv'), encoding='utf-8')


        return 1


    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        return output_response


if __name__ == "__main__":
    pred = Prediction()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
