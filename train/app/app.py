"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
#from sklearn.linear_model import LinearRegression
#from sklearn.preprocessing import PolynomialFeatures

import xgboost as xgb
import numpy as np
import pandas as pd
import os
import pickle

import json

__author__ = "### Author ###"

logger = XprLogger("train",level=logging.INFO)


class Train(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="Train")
        """ Initialize all the required constansts and data her """
        self.mount_path = "/data/"

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        file_path = os.path.join(self.mount_path, 'covid19_train_processed.xlsx')
        df = pd.read_excel(file_path)

        countries = df.Country.unique()

        model_dict = {}
        for country in countries:
            country_data = df[df.Country == country]
            country_data = country_data.reset_index(drop=True)
            x_data = np.array(country_data.index)
            # x_data = pd.DataFrame(country_data.index)
            x_data = np.reshape(x_data, (x_data.shape[0], 1))
            y_data = country_data.Confirmed

            xgb_reg = xgb.XGBRegressor(n_estimators=200, learning_rate=0.08, gamma=0,
                                       subsample=0.75, colsample_bytree=1, max_depth=4)
            xgb_reg.fit(x_data, y_data)
            model_dict[country] = xgb_reg

        print("Models Created")

        country_data.reset_index(inplace=True, drop=True)
        last_date = country_data['Date'].tail(1).tolist()
        last_index = country_data.index[-1]

        # save the model to disk

        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)

        with open(os.path.join(self.OUTPUT_DIR,'boosted_models.sav'), 'wb') as f:
            pickle.dump(model_dict, f)


        # save metadata to the disk
        with open(os.path.join(self.OUTPUT_DIR, "meta.json"), 'w') as f:
            json.dump({'last_date': str(last_date), 'last_index': last_index}, f)

        print("Model dumped at {}".format(self.OUTPUT_DIR))

        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = Train()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
